﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MergeSort
{
    class Program
    {
        static void merge(int[] arr, int left, int mid, int right)
        {
            int i, j, k;
            int length0 = mid - left + 1;
            int length1 = right - mid;
            int[] L = new int[length0];
            int[] R = new int[length1];

            for (i=0;i<length0;i++)
            {
                L[i] = arr[left + i];
            }
            for (j=0;j<length1;j++)
            {
                R[j] = arr[mid + j + 1];
            }

            i = 0;
            j = 0;
            k = left;
            while (i<length0 && j<length1)
            {
                if (L[i] <= R[j])
                {
                    arr[k] = L[i];
                    i++;
                }
                else
                {
                    arr[k] = R[j];
                    j++;
                }
                k++;
            }

            while (i < length0)
            {
                arr[k] = L[i];
                i++;
                k++;
            }

            while (j < length1)
            {
                arr[k] = R[j];
                j++;
                k++;
            }
        }

        static void sort(int[] arr, int left, int right)
        {
            if (left < right)
            {
                int mid = (left + right) / 2;

                sort(arr, left, mid);
                sort(arr, mid + 1, right);
                merge(arr, left, mid, right);
            }
        }

        static void Main(string[] args)
        {
            bool exitApp = false;

            while (!exitApp)
            {
                Console.WriteLine("Enter an integer between 5 and 50 to specify the size of an array to be generated. Enter any other key to exit.");
                string read = Console.ReadLine();
                int length = 0;
                int.TryParse(read, out length);
                if (length < 5 || length > 50)
                {
                    exitApp = true;
                    break;
                }

                int i;
                int[] arr = new int[length];
                string[] arrText = new string[length];
                var ran = new Random();

                int n;
                string totalText = "";
                for (i = 0; i < length; i++)
                {
                    n = ran.Next(0, 100);
                    arr[i] = Convert.ToInt32(n);
                    if (i > 0)
                    {
                        totalText += ", ";
                    }
                    totalText += Convert.ToString(n);
                }

                Console.WriteLine("Here is the unsorted array: " + totalText);
                Console.WriteLine("Enter any key to sort the array...");
                Console.ReadLine();

                sort(arr, 0, length-1);

                totalText = "";
                for (i = 0; i < length; i++)
                {
                    if (i > 0)
                    {
                        totalText += ", ";
                    }
                    totalText += Convert.ToString(arr[i]);
                }
                Console.WriteLine("Here is the array after being sorted: " + totalText);
                Console.ReadLine();
            }
        }
    }
}
